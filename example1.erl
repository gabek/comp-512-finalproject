-module(example1).

-export([sum_of_list/1]).

sum_of_list([]) -> 0;

sum_of_list([Head|List]) ->
		Head + sum_of_list(List).
