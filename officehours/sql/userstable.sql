DROP TABLE IF EXISTS users;

CREATE TABLE users (
	username varchar(255) UNIQUE NOT NULL PRIMARY KEY,
	firstname varchar(255) NOT NULL,
	lastname varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	user_type varchar(255) NOT NULL
);
