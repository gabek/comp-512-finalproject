DROP TABLE IF EXISTS teacher_hours;

CREATE TABLE teacher_hours (
	username varchar(255) UNIQUE NOT NULL REFERENCES users(username),
	monday_start varchar(255),
	monday_end varchar(255),
	tuesday_start varchar(255),
	tuesday_end varchar(255),
	wednesday_start varchar(255),
	wednesday_end varchar(255),
	thursday_start varchar(255),
	thursday_end varchar(255),
	friday_start varchar(255),
	friday_end varchar(255)
);
