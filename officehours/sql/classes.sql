DROP TABLE IF EXISTS classes;

CREATE TABLE classes(
	class varchar(255) UNIQUE NOT NULL PRIMARY KEY,
	teacher varchar(255) NOT NULL REFERENCES users(username)
);
