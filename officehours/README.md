# Officehours
=====

An OTP Application for Penn State COMP 512. It is a webchat meets voice over IP similar to Skype or Discord.

It is designed with a classroom environment in mind and should be used there. 


## Documentation
----- 
Documentation about the Server can be found [here](./doc/index.html) when opening from desktop.

The documentation is created in the [EDoc format](http://erlang.org/doc/apps/edoc/chapter.html).

## Requirements

### Client
------

* [Mozilla Firefox Browser](https://www.mozilla.org/en-US/firefox/) is all that is needed.
	* [Google Chrome](https://www.google.com/chrome/) can also work if the user doesn't need to use Voice over IP.

### Server
------

The following items are needed to install and run the server.

* [Erlang/OTP](https://www.erlang.org/) Version 21+.
	* To Install follow the instructions [here](http://erlang.org/doc/installation_guide/INSTALL.html).
* [Open SSL](https://www.openssl.org/).
* [PostgreSQL](https://www.postgresql.org/).
* One of the following Unix Based Distros:
	* [Linux](https://www.linux.com/what-is-linux)
		* Testing was on a Linux distro and will get you the best results.
	* [Darwin](https://opensource.apple.com/)
	* [Solaris](https://www.oracle.com/solaris/solaris11/)
	* [GNU](https://www.gnu.org/)
	* [FreeBSD](https://www.freebsd.org/)
	* [NetBSD](https://netbsd.org/)
	* [OpenBSD](https://www.openbsd.org/)
	* [DragonFly](http://www.dragonflybsd.org/)
	* [Msys2](https://www.msys2.org/)


## Install and Build
-----

Either clone the repo, or download the source code.

```
    git clone git@bitbucket.org:gabek/comp-512-finalproject.git
```

Meet the requirements in Server. 

Go to comp-512-finalproject/officehours and run the following commands.

    $ make
    $ make run


## Usage

### Starting The Application
----
* Use the make run command on the server.

````
$ make run
````

* Go to specified IP address (based on server's ip).

### Logging In
----

* Click on connect to the server by clicking the connect button.
* Logging in requires a Username, a Password, and a valid Class.
* If you have no account switch to the create user view. 
	* Provide a First name, Last Name, Password, and an Id. The First and Last Name as well as Id will be used to generate a username.
	* Create user can only create a student. If you wish to make your account a teacher


### Available to All Accounts
----

* You should see a Gray text box with a text field and a button that says 'send' underneath.
	* Entering text in the box and clicking send will send a message to the chatroom.
	* When a message is sent to the chatroom you will see it in the Gray text box.
* To change passwords click the change passwords button.
	* A new container should open up, with three fields: current password, new password, confirm new password.
	* Insert your current password into the current password field.
	* Insert your new password in the other two fields.
* To Logout click on the logout button.
	* The view should fade and take you back to the login page.

### Student Accounts only.
----

* Students have two unique buttons: check office hours, and call teacher.
* Check officehours will get today's officehours from the database.
* Call teacher will call the teacher to set up voice communications.
	* If successful you will be prompted for a microphone.
	* If the teacher is busy (in another call), a message will be displayed saying so.
	* If the communication fails at any point a message will be displayed saying so. 


### Teacher Accounts only.
----

* Teachers have two unique containers to them: the modify office hours container, and modify class container.
* The modify hours container is available upon opening a teacher view. 
	* A drop down field with the values "Monday", "Tuesday", "Wednesday", "Thursday", and "Friday". This is used to select the day you want to modify.
	* The start time field denotes when the officehour starts, and the end time field denotes when the offiechours ends.
		* The start time must be less than the end time, otherwise it won't be accepted.
	* Clicking the submit hours button will submit the information to the database.
	* Submitting an empty time or malformed time is interpreted as no officehours today.
* The modify class container can be opened by clicking on the modify class button.
	* It has two different forms: add class, and remove class.
	* Add class will add a new class to the database with you as the teacher.
	* Delete class will remove a class in the database where you are the teacher.
		* When a class is deleted, a message will be sent to any user currently in the class to let them know that the class is no longer valid.
* At any point the teacher may receive a popup from the student, requesting a call.
	* The teacher can either accept the call and start voice communications or deny the call and leave it at that.

