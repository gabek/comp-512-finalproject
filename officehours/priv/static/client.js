/**
 * @Author Gabe Kelly
 * 4/21/2019
 * COMP 512
 * The client side javascript code.
 * Initially based on erlbus chat example at:
 * https://github.com/cabol/erlbus/tree/master/examples/chat
 *
 * Audo Signaling and RTCPeer connection based on tutorial at:
 * https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Signaling_and_video_calling
 *
**/

var websocket;
var user;
var myPeerConnection = null;
var target = null;
var incomingAudio = null;
var audioContext = null;
var teacher = false;

jQuery(document).ready(init);

/**
 * Initializes the HTML document. Also checks to see if websocket is allowed.
**/
function init()
{
    jQuery("#classContainer").hide();
    jQuery("#errorContainer").hide();
    jQuery("#messageContainer").hide();
    jQuery("#connectButton").hide();
    jQuery("#loginContainer").hide();
    jQuery("#createUserContainer").hide();
    jQuery("#logoutButton").hide();
    jQuery("#callButton").hide();
    jQuery("#checkHours").hide();
    jQuery("#endCall").hide();
    jQuery("#endContainer").hide();
    jQuery("#timeContainer").hide();
    jQuery('#changePassContainer').hide();
    jQuery('#modifyClassContainer').hide();
    jQuery('#modifyClassButton').hide();
    if (!("WebSocket" in window))
    {
        showError('The web browser you are using doesn\'t support websocket technology. Please switch to a browser compatible with officehours!');
    }
    else
    {
        jQuery("#connectButton").show();
    };
};

/**
 * Establishes connection to the websocket.
**/

function connect()
{
    wsHost = "wss://" + window.location.host + "/websocket";
    websocket = new WebSocket(wsHost);
    websocket.onopen = function(evt)
    {
        onOpen(evt)
    };
    websocket.onclose = function(evt)
    {
        onClose(evt)
    };
    websocket.onmessage = function(evt)
    {
	    onMessage(evt)
    };
    websocket.onerror = function(evt)
    {
        onError(evt)
    };
};

/**
 * A function that is called when the websocket opens.
**/

function onOpen(evt)
{
    jQuery('#loginContainer').fadeIn('slow');
    jQuery('#connectButton').fadeOut('slow');
};

/**
 * A function that is called when the websocket closes.
**/

function onClose(evt)
{
    target = "";
    user = "";
    cleanClassContainer();
    jQuery('#loginContainer').fadeOut('slow');
    jQuery('#logoutButton').fadeOut('slow');
    jQuery('#checkHours').fadeOut('slow');
    jQuery('#createUserContainer').fadeOut('slow');
    if (jQuery('#endCall').css("visibility") == "visible")
    {
        cleanUpCall();
    }
    jQuery('#callButton').fadeOut('slow');
    jQuery('#timeContainer').fadeOut('slow');
    jQuery('#currentPassword').val("");
    jQuery('#newPassword').val("");
    jQuery('#confirmNewPass').val("");
    jQuery("#changePassContainer").fadeOut('slow');
    jQuery('#modifyClassContainer').fadeOut('slow');
    jQuery('#addition').val("");
    jQuery('#removal').val("");
    jQuery('#modifyClassButton').fadeOut('slow');
    jQuery('#changeButton').fadeOut('slow');
    jQuery('#connectButton').fadeIn('slow');
    jQuery("#messageContainer").hide();
    showError('Connection to the server has been severed.');
};

/**
 * A function that is called whenever the websocket
 * receives a message from the server.
**/

function onMessage(evt)
{

    var response = jQuery.parseJSON(evt.data);
    console.log(response);
    if (response.type == "message")
    {
        showMessage(jQuery.parseJSON(evt.data));
    }
    else if (response.type == "callstart")
    {
        beginCall();
    }
    else if (response.type == "login_response")
    {
        handleLogin(response);
    }
    else if (response.type == "request_call")
    {
        if (myPeerConnection == null)
        {
            if (window.confirm("Accept call from " + response.student_name + "?"))
            {
                callStudent(response.student_name);
            }
            else
            {
                console.log("Denied");
            };
        }
        else
        {
            showServerMessage('Received call request from ' + response.student_name + '!');
            sendToServer("busy", response.student_name);
        };
    }
    else if (response.type == "new_ice_candidate")
    {
        console.log(response);
        handleIceCandidate(response);
    }
    else if (response.type == "call_offer")
    {
        console.log(response.sdp);
        answerCallOffer(response);
    }
    else if (response.type == "call_answer")
    {
        console.log(response);
        handleAnswer(response);
    }
    else if (response.type == "end_call")
    {
        cleanUpCall();
    }
    else if (response.type == "is_teacher")
    {
        teacher = true;
    }
    else if (response.type == "hours_submit")
    {
        console.log(response);
    }
    else if (response.type == "server_message")
    {
        showServerMessage(response.text);
    }
    /*else if (response.type == "switch_class")
    {
    }*/
    else
    {
        console.log('Websocket received an unknown message:' + response.type + '!');
    };
};

/**
 * Sends a message to the chat room.
**/

function sendMessage()
{
    if (websocket.readyState == websocket.OPEN)
    {
        var txt = jQuery("#classRoomMessage").val();
	var value = {text:txt, user:user};
        sendToServer("message", value);
        jQuery("#classRoomMessage").val("");
    }
    else
    {
        showError('ERROR: websocket is not ready');
    };
};

/**
 * Displays messages sent to the classroom.
 * Will display up to 100 messages from the server.
**/

function showMessage(data)
{
    jQuery('#chatContainer').append('<div>' + data.sender + ' -> ' + data.msg + '</div>');
    console.log(jQuery('#chatContainer')[0]);
    if (jQuery('#chatContainer')[0].childElementCount > 100)
    {
        var removal = jQuery('#chatContainer')[0].firstChild;
        jQuery('#chatContainer')[0].removeChild(removal);
    };
};

/**
 * Sends information from the client to the server to login.
**/

function login()
{
    var username = jQuery("#username").val();
    user = username;
    var password = jQuery("#password").val();
    var classroom = jQuery("#class").val();
    if (username == "" || password == "" || classroom == "")
    {
        showError('ERROR: need to enter Data Values');
    }
    else
    {
        var value = {
            username: username,
            pass: password,
            classroom: classroom
        };
        sendToServer("login", value);
        jQuery('#username').val("");
        jQuery('#password').val("");
        jQuery('#class').val("");
        showServerMessage('Sending login information. Please wait.');
    };
};

/**
 * Confirms the result of the login attempt.
**/

function handleLogin(response)
{
    var message = response.message;
    if(parseLogin(message))
    {

        jQuery("#loginContainer").fadeOut('slow');
        jQuery("#classContainer").fadeIn('slow');
        var htmlText = '<span>The current classroom is: ' + response.classroom + '</span>';
        jQuery("#className").html(htmlText);
        jQuery("#className").show();
        if (teacher == false)
        {
            jQuery("#callButton").fadeIn('slow');
            jQuery('#checkHours').fadeIn('slow');
        }
        else
        {
            jQuery("#timeContainer").fadeIn('slow');
            jQuery("#modifyClassButton").fadeIn('slow');
        }
        jQuery("#logoutButton").fadeIn('slow');
        showServerMessage('Successful Login');
    }
    else
    {
        user = "";
        showError('Login attempt failed: ' + message);
    }
};

/**
 * Parses the result from the login.
**/

function parseLogin(message)
{
    if (message == "good_login")
    {
        return true;
    }
    else
    {
        return false;
    };
};

/**
 * Sends information to the server to create a user.
 * Will not send if passwords don't match.
**/

function createUser()
{
    if (websocket.readyState == websocket.OPEN)
    {
        var firstname = jQuery('#firstname').val();
        var lastname = jQuery('#lastname').val();
        var password = jQuery('#createPass').val();
	var id = jQuery('#userId').val().toString();
        var confirm = jQuery('#confirmPass').val();
        if (firstname == "" || lastname == "" || password == "")
        {
            showServerMessage("Fill out all fields");
            return;
        }
        if (password.localeCompare(confirm) == 0)
        {
            var creation = {
                first: firstname,
                last: lastname,
                pass: password,
		id:id
            };
            sendToServer("create", creation);
        }
        else
        {
            showError('WARNING: Passwords didn\'t match. Please make sure they match and try again!');
        };
        jQuery('#firstname').val("");
        jQuery('#lastname').val("");
        jQuery('#createPass').val("");
	jQuery('#userId').val("1");
        jQuery('#confirmPass').val("");
    }
    else
    {
        showError('ERROR: disconnected from server!');
    };
};

/**
 * Calls the server to logout the user.
**/

function logout()
{
    if (websocket.readyState == websocket.OPEN)
    {
        sendToServer("logout", "logout");
	user = "";
	target = "";
	cleanClassContainer();
	cleanUpCall();
 	jQuery('#logoutButton').fadeOut('slow');
	jQuery('#callButton').fadeOut('slow');
 	jQuery('#timeContainer').fadeOut('slow');
	jQuery('#checkHours').fadeOut('slow');
	jQuery('#currentPassword').val("");
	jQuery('#newPassword').val("");
 	jQuery('#confirmNewPass').val("");
 	jQuery("#changePassContainer").fadeOut('slow');
	jQuery('#modifyClassContainer').fadeOut('slow');
    	jQuery('#modifyClassButton').fadeOut('slow');
    	jQuery('#addition').val("");
    	jQuery('#removal').val("");
    	jQuery('#loginContainer').fadeIn('slow');
    	teacher = false;
    }
    else
    {
        showError('ERROR: disconnected from server');
    };
};

/**
 * Switches the users classes to the specified value.
**/

function switchClass()
{
    var classId = jQuery("#classId").val();
    if(classId == "")
    {
        showMessage("Please enter a valid class name");
        return;
    }
    if(websocket.readyState == websocket.OPEN)
        sendToServer("switch-class", classId);
    else
        showError("ERROR: no webserver connection");
}

function changeClasses(classResponse)
{
    console.log(classResponse);
    /*
    if(classResponse.result == "success")
    {
        cleanClassContainer();
        var htmlText = '<span>The current classroom is: ' + classResponse.classroom + '</span>';
        jQuery("#className").html(htmlText);
    }
    else
    {
        showError("Bad class, please enter a different value.");
    }*/
}

/**
 * Contacts the teacher to initiate a call.
**/

function contactTeacher()
{
    if (websocket.readyState == websocket.OPEN)
    {
        audioContext = new AudioContext();
        sendToServer("initiate_call", user);
    }
    else
    {
        showError('ERROR: no webserver connection');
    };
};

/**
 * Calls the student and provides them with an offer.
**/

function callStudent(studentName)
{
    if (websocket.readyState == websocket.OPEN)
    {
        audioContext = new AudioContext();
        if (myPeerConnection == null)
        {
            target = studentName;
            createPeerConnection();
            navigator.mediaDevices.getUserMedia(
            {
                audio: true
            }).then(
                function(stream)
                {
                    var source = audioContext.createMediaStreamSource(stream);
                    var destination = audioContext.createMediaStreamDestination();
                    source.connect(destination);
                    destination.stream.getTracks().forEach(track => myPeerConnection.addTrack(track, destination.stream));
                }).catch(function(err)
            {
                showError('ERROR: error with audio device');
            });
        }
        else
        {
            console.log("WARNING call already in progress!");
            showServerMessage('WARNING call already in progress!');
        }
    }
    else
    {
        console.log("ERROR: Bad connection!");
        showError('ERROR: Disconnected from server!');
    };
};

/**
 * Creates a peer connection for voice communication.
**/

function createPeerConnection()
{
    myPeerConnection = new RTCPeerConnection(
    {
        iceServers: [
        {
            urls: "stun:stun.iptel.org"
        },
        {
            urls: "stun:stun.l.google.com:19302"
        }]
    });
    console.log("ICE server established");

    myPeerConnection.onicecandidate = function(event)
    {
        if (event.candidate)
        {
            var value = {
                name: user,
                target: target,
                candidate: event.candidate
            };
            sendToServer("candidate", value);
        }
    };

    myPeerConnection.ontrack = function(event)
    {
        jQuery('#endCall').fadeIn('slow');
        jQuery('#callButton').fadeOut('slow');
        var mediaStream = new MediaStream(event.streams[0]);
        incomingAudio = audioContext.createMediaStreamSource(mediaStream);
        incomingAudio.connect(audioContext.destination);
    };

    myPeerConnection.onsignalingstatechange = function(event)
    {
     	console.log("Registered: " + event);
        switch (myPeerConnection.signalingState)
        {
            case "closed":
                console.log("test disconnect");
		cleanUpCall();
                break;
        }
    };
    myPeerConnection.onnegotiationneeded = function(event)
    {
        handleNegotiation();
    };
};

/**
 * Handles a negotioation from another user.
**/

function handleNegotiation()
{
    myPeerConnection.createOffer().then(function(offer)
    {
        return myPeerConnection.setLocalDescription(offer);
    }).then(function()
    {
        var connection = {
            name: user,
            target: target,
            sdp: myPeerConnection.localDescription
        };
        console.log(connection);
        sendToServer("call-offer", connection);
    }).catch(function(err)
    {
        console.log(err);
        console.log(err.message)
        showError('ERROR: bad negotiation with target peer');
        sendToServer("partial-failure", target);
        cleanUpCall();
    });
};

/**
 * Handles ice candidates coming from the other user.
**/

function handleIceCandidate(iceCandidate)
{
    if (myPeerConnection != null)
    {
        var candidate = new RTCIceCandidate(iceCandidate.candidate);
        myPeerConnection.addIceCandidate(candidate).catch(function(error)
        {
            console.log(error);
            console.log(error.message);
        });
    }
    else
        console.log("No peer connection");

};

/**
 * Sends the peer a answer in response to a call offer.
**/

function answerCallOffer(callOffer)
{
    target = callOffer.from;
    console.log(target);
    if (myPeerConnection == null)
        createPeerConnection();
    var description = new RTCSessionDescription(callOffer.sdp);
    myPeerConnection.setRemoteDescription(description).then(function()
    {
        return navigator.mediaDevices.getUserMedia(
        {
            audio: true
        })
    }).then(function(stream)
    {
        var source = audioContext.createMediaStreamSource(stream);
        var destination = audioContext.createMediaStreamDestination();
        source.connect(destination);
        destination.stream.getTracks().forEach(track => myPeerConnection.addTrack(track, destination.stream));
    }).then(function()
    {
        return myPeerConnection.createAnswer();
    }).then(function(answer)
    {
        return myPeerConnection.setLocalDescription(answer);
    }).then(function()
    {
        console.log("SDP");
        console.log(myPeerConnection.localDescription);
        var value = {
            name: user,
            target: target,
            sdp: myPeerConnection.localDescription
        };
        sendToServer("call-answer", value);
    }).catch(function(error)
    {
        showError('ERROR: failed to answer call');
        console.log(error);
        console.log(error.message);
        sendToServer("partial-failure", target);
        cleanUpCall();
    });
};

/**
 * Handles a call answer coming from a user.
**/

function handleAnswer(answer)
{
    var description = new RTCSessionDescription(answer.sdp);
    myPeerConnection.setRemoteDescription(description);
};


/**
 * Moves to the create user panel.
**/

function goToCreateUser()
{
    jQuery("#loginContainer").fadeOut('slow');
    jQuery("#createUserContainer").fadeIn('slow');
};

/**
 * Returns to the login panel.
**/

function returnToLogin()
{
    jQuery('#firstname').val("");
    jQuery('#lastname').val("");
    jQuery('#createPass').val("");
    jQuery('#confirmPass').val("");
    jQuery("#createUserContainer").fadeOut('slow');
    jQuery("#loginContainer").fadeIn('slow');
};

/**
 * Submit hours to the server.
**/

function submitHours()
{
    var select = document.getElementById('day');
    var day = select.options[select.selectedIndex].value;
    var start = jQuery('#start_time').val();
    var end = jQuery('#end_time').val();
    if (start != null && start.localeCompare(end) > 0)
    {
        showError('ERROR: Start time must be before endtime');
        return;
    }
    var value = {
        day: day,
        starttime: start,
        endtime: end,
        username: user
    };
    console.log("Submitting Hours");
    console.log(value);
    if (websocket.readyState == websocket.OPEN)
    {
        sendToServer("submit_hours", value);
    }
    else
    {
        showError('ERROR: Websocket disconnected from Server');
    };
};

/**
 * Check hours from the server.
**/

function checkHours()
{
    if (websocket.readyState == websocket.OPEN)
    {
        var date = new Date();
        var day = date.getDay();
        sendToServer("check_hours", day);
    }
    else
    {
        showError('ERROR: Lost Websocket connection');
    };
};

/**
 * End the voice call.
**/

function endCall()
{
    cleanUpCall();
    if (websocket.readyState == websocket.OPEN)
    {
        sendToServer("end-call", target);
    }
    else
    {
        showError('ERROR: failed to send server clean up call message!');
    };
    target = null;
};

/**
 * Cleans up the call when it is done.
**/

function cleanUpCall()
{
    if (myPeerConnection != null)
    {
        myPeerConnection.ontrack = null;
        myPeerConnection.onicecandidate = null;
        myPeerConnection.onnegotiationneeded = null;
        myPeerConnection.onconnectionstatechange = null;

        if (incomingAudio != null && incomingAudio.mediaStream != null)
        {
            incomingAudio.mediaStream.getTracks().forEach(track => track.stop());
        }

        incomingAudio = null;
        myPeerConnection.close();
        myPeerConnection = null;
    }

    if (audioContext != null)
        audioContext.close();
    jQuery('#endCall').fadeOut('slow');
    if (websocket.readyState == websocket.OPEN && teacher == false)
        jQuery('#callButton').fadeIn('slow');
};

/**
 * Open the modify class container.
**/

function openModifyClassContainer()
{
    jQuery('#classContainer').fadeOut('slow');
    jQuery('#modifyClassButton').fadeOut('slow');
    jQuery('#timeContainer').fadeOut('slow');
    jQuery('#modifyClassContainer').fadeIn('slow');
};

/**
 * Add a new class to the database.
**/

function addClassroom()
{
    var add = jQuery('#addition').val();
    var addition = {
        classroom: add,
        username: user
    };
    if (add == "")
    {
        showServerMessage("Please enter the name of the class");
        return;
    }
    if (websocket.readyState == websocket.OPEN)
    {
        sendToServer("add-class", addition);
    }
    else
    {
        showError('ERROR: failed to contact server!');
    };
    jQuery('#addition').val("");
};

/**
 * Remove a class from the database.
**/

function removeClassroom()
{
    var remove = jQuery('#removal').val();
    if (remove == "")
    {
        showServerMessage("Please enter the name of the class");
        return;
    }
    if (websocket.readyState == websocket.OPEN)
    {
	var value = {classroom:remove, username:user};
        sendToServer("remove-class", value);
    }
    else
    {
        showError('ERROR: failed to contact server!');
    };
    jQuery('#removal').val("");
};

/**
 * Close the modify class container
**/

function closeModifyClassContainer()
{
    jQuery('#modifyClassContainer').fadeOut('slow');
    jQuery('#timeContainer').fadeIn('slow');
    jQuery('#classContainer').fadeIn('slow');
    jQuery('#modifyClassButton').fadeIn('slow');
};

/**
 * Open the change password container.
**/

function openChangeContainer()
{
    jQuery("#classContainer").fadeOut('slow');
    jQuery('#timeContainer').fadeOut('slow');
    jQuery('#modifyClassButton').fadeOut('slow');
    jQuery("#changePassContainer").fadeIn('slow');
};

/**
 * Submit information to change the password.
**/

function changePassword()
{
    if (websocket.readyState == websocket.OPEN)
    {
        var current = jQuery('#currentPassword').val();
        var newPassword = jQuery('#newPassword').val();
        var confirm = jQuery('#confirmNewPass').val();
        if (current == "" || newPassword == "")
        {

            showServerMessage("Please enter the current and new passwords please");
            return;
        }
        if (newPassword.localeCompare(confirm) == 0)
        {
            var change = {
                username: user,
                current: current,
                new_password: newPassword
            }
            sendToServer("change", change);
        }
        else
        {
            showError('WARNING: Passwords didn\'t match. Please make sure they match and try again.');
        };
        jQuery('#currentPassword').val("");
        jQuery('#newPassword').val("");
        jQuery('#confirmNewPass').val("");
    }
    else
    {
        showError('ERROR: disconnected from server');
    };
};

/**
 * Close the change password container.
 **/

function closeChangeContainer()
{
    jQuery('#currentPassword').val("");
    jQuery('#newPassword').val("");
    jQuery('#confirmNewPass').val("");
    jQuery("#changePassContainer").fadeOut('slow');
    if (teacher == true)
    {
        jQuery('#modifyClassButton').fadeIn('slow');
        jQuery('#timeContainer').fadeIn('slow');
    }
    jQuery("#classContainer").fadeIn('slow');
};

/**
 * Send json data to the server.
**/

function sendToServer(typeVar, valueVar)
{
    var message = {
        type: typeVar,
        value: valueVar
    };
    var msgJson = JSON.stringify(message);
    websocket.send(msgJson);
};

/**
 * Clean the the classroom container.
**/

function cleanClassContainer()
{
    jQuery('#classContainer').fadeOut('slow');
    while (jQuery('#chatContainer')[0].firstChild != null)
    {
        var removal = jQuery('#chatContainer')[0].firstChild;
        jQuery('#chatContainer')[0].removeChild(removal);
    }
};

/**
 * Display a message from the server.
**/

function showServerMessage(message)
{
    var htmlText = '<span style="color: blue;">' + message + '</span>';
    jQuery("#messageContainer").html(htmlText);
    jQuery("#messageContainer").show();
};

/**
 * Show an error or warning message from the server.
**/

function showError(error)
{
    var htmlText = '<span style="color: red;">' + error + '</span>';
    jQuery("#errorContainer").html(htmlText);
    jQuery("#errorContainer").show();
}
