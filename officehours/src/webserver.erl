%%%----------------------------------------------------------------------
%%% @doc The backend server for officehours. Handles connections to the
%%% database, and any passing of information to and from clients.
%%% Uses the gen_server behavior as the basis of the calls.
%%%
%%% @see gen_server
%%% 
%%% @todo fix create student and teacher
%%%
%%% @author Gabe Kelly
%%% @end
%%%----------------------------------------------------------------------

-module(webserver).
-behaviour(gen_server).

%%----------------------------------------------------------------------
%% API functions
%%----------------------------------------------------------------------
-export([start_link/0, lookup_username/1, create_teacher/5, create_student/4,login/4,
logout/1, switch_class/1, contact_teacher/2, end_call/1, handle_ice_candidate/4, call_offer/3, call_answer/2,
submit_hours/4, check_hours/2, change_password/3, contact_peer/2, add_class/2, remove_class/2]).

%%----------------------------------------------------------------------
%% GenServer Call back
%%----------------------------------------------------------------------
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, handle_continue/2, terminate/2, code_change/3]).


%%----------------------------------------------------------------------
%% @hidden Start link. Starts a local gen_server and connects to it.
%%
%% @see gen_server:start_link/0
%% @end
%%----------------------------------------------------------------------
start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [],[]).

%%----------------------------------------------------------------------
%% @doc A wrapper for logging in a user. Checks for correct password and username
%% combo as well as a valid class.
%%
%% @end
%%----------------------------------------------------------------------

-spec login(string(), string(), string(), string()) -> atom().

login(Ip, Username, Password, Class) ->
	gen_server:call(?MODULE, {login, Ip, Username, Password, Class}).

%%----------------------------------------------------------------------
%% @doc A wrapper for logging out a user. Takes in a string for the users
%% ip.
%%
%% @end
%%----------------------------------------------------------------------

-spec logout(string()) -> ok.

logout(Ip) ->
	gen_server:cast(?MODULE, {logout, Ip}).

%%----------------------------------------------------------------------
%% @doc A wrapper for switching the class the user is in.
%%
%% @end
%%----------------------------------------------------------------------

-spec switch_class(string()) -> succes | failure.

switch_class(ClassId) ->
	gen_server:call(?MODULE, {switch_class, ClassId}).
%%----------------------------------------------------------------------
%% @doc A wrapper for looking up the username based on the ip. 
%%
%% @deprecated 
%% @end
%%----------------------------------------------------------------------

-spec lookup_username(string()) -> [{string(), {string(), pid()}}].

lookup_username(Ip) ->
	gen_server:call(?MODULE, {lookup_username, Ip}).

%%----------------------------------------------------------------------
%% @doc A wrapper for creating a teacher user. Requires a first name, lastname,
%% password, id number, and a list of available classes.
%%
%% @end
%%----------------------------------------------------------------------

-spec create_teacher(string(), string(), string(), string(), list()) -> {ok, integer()} | {error, string()}.

create_teacher(First, Last, Password,IdNum, Classes) ->
	gen_server:call(?MODULE, {create_teacher, First, Last, Password,IdNum, Classes}).

%%----------------------------------------------------------------------
%% @doc A wrapper for creating a student user. Requires a firstname, lastname,
%% id number, and a password.
%%
%% @end
%%----------------------------------------------------------------------

-spec create_student(string(), string(), string(), string()) -> string().

create_student(First, Last, Password, IdNum) ->
	gen_server:call(?MODULE, {create_student, First, Last, Password, IdNum}).

%%----------------------------------------------------------------------
%% @doc A wrapepr for contacting a teacher to initiate a call. Requires an
%% Username, and classroom.
%%
%% @end
%%----------------------------------------------------------------------

-spec contact_teacher(string(), string()) -> binary().

contact_teacher(Username, Class) ->
	gen_server:call(?MODULE, {contact_teacher, Username, Class}).

%%----------------------------------------------------------------------
%% @doc A wrapper for passing Ice candidates back and forth between users.
%% Requires a Username, the user it is going to, the ice candidate, and the
%% PID of who it is from.
%% 
%% @private
%% @end
%%----------------------------------------------------------------------

handle_ice_candidate(Username, Target, Candidate, From) ->
	gen_server:cast(?MODULE, {handle_ice_candidate, Username, Target, Candidate, From}).

%%----------------------------------------------------------------------
%% @doc A wrapper for handling a call offer to a client peer.
%% Requires a username, a target, and a SDP.
%%
%% @private
%% @end
%%----------------------------------------------------------------------

call_offer(Username, Target, SDP) ->
	gen_server:call(?MODULE, {call_offer, Username, Target, SDP}).

%%----------------------------------------------------------------------
%% @doc A wrapper for handling an answer in response to an offer.
%%
%% @private
%% @end
%%----------------------------------------------------------------------

call_answer(Target, SDP) ->
	gen_server:call(?MODULE, {call_answer, Target, SDP}).

%%----------------------------------------------------------------------
%% @doc A wrapper for ending a call with another client. Requires
%% just the target of who needs to end a call.
%% 
%% @private
%% @end
%%----------------------------------------------------------------------

end_call(Target) ->
	gen_server:cast(?MODULE, {end_call, Target}).

%%----------------------------------------------------------------------
%% @doc A wrapper for submitting hours to the database. Requires the day,
%% the start time of the officehours, and the end time of the officehours. 
%% 
%% @private
%% @end
%%----------------------------------------------------------------------

submit_hours(Day, Start, End, Username) ->
	gen_server:call(?MODULE, {submit_hours, Day, Start, End, Username}).

%%----------------------------------------------------------------------
%% @doc A wrapper for checking what the current hours are in the database.
%% Requires an integer for the day, and the class it is for.
%%
%% @end
%%----------------------------------------------------------------------

-spec check_hours(integer(), string()) -> binary().

check_hours(DayNum, Class) ->
	gen_server:call(?MODULE, {check_hours, DayNum, Class}).

%%----------------------------------------------------------------------
%% @doc A wrapper for changing a users password. Requires the username, 
%% current password, and the new password.
%%
%% @end
%%----------------------------------------------------------------------

-spec change_password(string(), string(), string()) -> binary().

change_password(Username, CurrentPass, NewPass) ->
	gen_server:call(?MODULE, {change_password, Username, CurrentPass, NewPass}).

%%----------------------------------------------------------------------
%% @doc A wrapper for contacting another client. Requires the target, and a 
%% message.
%%
%% @end
%%----------------------------------------------------------------------

-spec contact_peer(string(), string()) -> ok.

contact_peer(Target, Message) ->
	gen_server:cast(?MODULE, {contact_peer, Target, Message}).

%%----------------------------------------------------------------------
%% @doc A wrapper for adding a new class to the database. Requires the
%% new class, and the teacher who is adding it.
%%
%% @private
%% @end
%%----------------------------------------------------------------------

add_class(Class, Username) ->
	gen_server:call(?MODULE, {add_class, Class, Username}).

%%----------------------------------------------------------------------
%% @doc A wrapper for removing a class from the database. Requires the class
%% name to be removed, and the teacher who is removing it.
%%
%% @private
%% @end
%%----------------------------------------------------------------------

remove_class(Class, Username) ->
	gen_server:call(?MODULE, {remove_class, Class, Username}).

%%----------------------------------------------------------------------
%% Genserver callbacks
%% We need these to complete the gen_server behavior.
%%---------------------------------------------------------------------- 

%%----------------------------------------------------------------------
%% @doc Initiates the webserver and the usernames and logged on table.
%% Called automatically on startup.
%% 
%% @see gen_server:init/2
%% @private
%% @end
%%----------------------------------------------------------------------
init([]) ->
	ets:new(usernames, [set, private, named_table]),
	ets:new(logged_on, [set, private, named_table]),
	{ok, []}.

%%----------------------------------------------------------------------
%% @doc Synchronous calls from the webserver. Is where the meat of the calculations
%% are performed. Takes in the call, who its from, and the current state.
%% 
%% @see gen_server:handle_call/3
%% @private
%% @end
%%----------------------------------------------------------------------
 
handle_call({create_teacher, First, Last, Password, IdNum, Classes}, _, State) ->
	Username = string:concat(string:concat(First, Last), IdNum),
	{ok, Salt} = bcrypt:gen_salt(),
	{ok, Hash} = bcrypt:hashpw(Password, Salt),
	Connect = connect_sql(),
	Reply = epgsql:equery(Connect, "insert into users(username, firstname, lastname, password, user_type) values($1, $2, $3, $4, $5)", [Username, First, Last, Hash, "Teacher"]),
	case Reply of
		{ok, _} ->
			epgsql:equery(Connect, "insert into teacher_hours(username) values((SELECT username FROM users WHERE username = $1))", [Username]),
			generate_classes(Connect, Classes, Username);
		_ -> ok
	end,
	epgsql:close(Connect),
	{reply, Reply, State};

%%----------------------------------------------------------------------
%% @hidden Creates a student user from the first name, lastname, Id, and password
%%----------------------------------------------------------------------

handle_call({create_student, First, Last, Password, IdNum}, _, State) ->
	Username = string:concat(string:concat(First, Last), IdNum),
	{ok, Salt} = bcrypt:gen_salt(),
	{ok, Hash} = bcrypt:hashpw(Password, Salt),
	Connect = connect_sql(),
	Result = epgsql:equery(Connect, "insert into users(username, firstname, lastname, password, user_type) values($1, $2, $3, $4, $5)", [Username, First, Last, Hash, "Student"]),
	epgsql:close(Connect),
	case Result of 
		{ok, _} -> Reply = string:concat("Created User: ", Username),
			{reply, Reply, State};
		_ -> Reply = "Failed to Create User",
			{reply, Reply, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Logs the user in. Specifically it calls compare login to handle
%% most of the work.
%%
%% @end
%%----------------------------------------------------------------------

handle_call({login, Ip, Username, Password, Class}, {PID,_}, State) ->
	Connect = connect_sql(),
	{ok, _, ClassResult} = epgsql:equery(Connect, "SELECT * FROM classes WHERE class = $1", [Class]),
	case ClassResult of 
		[] -> Reply = invalid_class,
		      epgsql:close(Connect),
		      {reply, Reply, State};
	       	_ -> 
		{ok, _, LoginData} = epgsql:equery(Connect, "SELECT password, user_type FROM users WHERE username = $1", [Username]), %%handle bad user maybe create new method within this?
		epgsql:close(Connect),	
		case LoginData of
			[] -> Reply = bad_login,
			      {reply, Reply, State};
			[{_,_}] ->
				[{Hash, Type}] = LoginData,
				Reply = compare_login(Password, Hash, Username, Ip, Type, PID),
				{reply, Reply, State};
			_ -> Reply = bad_login,
     		     		{reply, Reply, State}
		end
	end;

%%----------------------------------------------------------------------
%% @hidden Switches the class of the user.
%%----------------------------------------------------------------------

handle_call({switch_class, ClassId}, _From, State) ->
	Connect = connect_sql(),
	{ok, _, Data} = epgsql:equery(Connect, "SELECT class FROM classes WHERE class = $1", [ClassId]),
	epgsql:close(Connect),
	case Data of
		[{ClassId}] ->
			{reply, success, State};
		_ ->
			{reply, failure, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Looks up the username from the ip.
%% @deprecated
%% @end
%%----------------------------------------------------------------------

handle_call({lookup_username, Ip}, _From, State) ->
	Reply = ets:lookup(usernames, Ip),
	{reply, Reply, State};

%%----------------------------------------------------------------------
%% @hidden Contacts the teacher of the current class to start a call.
%%----------------------------------------------------------------------

handle_call({contact_teacher, Username, Class}, _, State) ->
	Connect = connect_sql(),
	{ok, _, Data} = epgsql:equery(Connect, "SELECT teacher FROM classes WHERE class = $1", [Class]),
	epgsql:close(Connect),
	case Data of 
		[{Teacher}] -> erlang:display(Teacher),
			Online = ets:lookup(logged_on, Teacher),
			case Online of 
				[{_,{_,TeacherPID}}] -> 
					TeacherPID ! {contact_teacher, Username},
					Reply = <<"Offered connection">>,
					{reply, Reply, State};
				_ ->
					Reply = <<"Teacher not online">>,
					{reply, Reply, State}
			end;
		_ ->
			Reply = <<"Not a valid class">>,
			{reply, Reply, State}
	end;
%%----------------------------------------------------------------------
%% @hidden Provides the target with a call offer. Traditionally from teacher to student.
%%----------------------------------------------------------------------

handle_call({call_offer, Username, Target, SDP}, {Pid,_}, State) ->
	TargetInfo = ets:lookup(logged_on, Target),
	case TargetInfo of
		[{_,_}] -> 
			[{_,{_,TargetPID}}] = TargetInfo,
			TargetPID ! {call_offer, Username, SDP},
			Reply = "SPD's offered",
			{reply, Reply, State};
		_ -> 
			Reply = "Callee not online. Terminating",
			Pid ! {end_call},
			{reply, Reply, State}
	end;

%%----------------------------------------------------------------------
%% @hidden An answer to a call offer. Traditionally from student to teacher.
%%----------------------------------------------------------------------

handle_call({call_answer, Target, SDP}, {Pid, _}, State) ->
	TargetInfo = ets:lookup(logged_on, Target),
	case TargetInfo of
		[{_,_}] -> 
			[{_,{_,TargetPID}}] = TargetInfo,
			TargetPID ! {call_answer, SDP},
			Reply = "SPD's answered",
			{reply, Reply, State};
		_ -> 
			Pid ! {end_call},
			Reply = "Caller not online. Terminating",
			{reply, Reply, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Handles the submission of new hours for a day.
%%----------------------------------------------------------------------

handle_call({submit_hours, Day, Start, End, Username}, _, State) ->
	try day_handler:day_field(Day) of
		{FieldStart, FieldEnd} -> 
			Connect = connect_sql(),
			erlang:display({FieldStart, FieldEnd}),
			Data = epgsql:equery(Connect, "UPDATE teacher_hours SET " ++ FieldStart ++ " = $1, " ++ FieldEnd ++ " = $2 WHERE username = $3", [Start, End, Username]),
			erlang:display(Data),
			epgsql:close(Connect),	
			{reply, updated_hours, State}
	catch
		Throw -> {reply, Throw, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Returns the hours for today.
%%----------------------------------------------------------------------
 
handle_call({check_hours, DayNum, Class}, _, State) ->
	Hours  = day_handler:get_day(DayNum),
	case Hours of
		{DayStart, DayEnd} -> 
			Connect = connect_sql(),
			erlang:display({DayStart, DayEnd}),
			{ok, _, Data} = epgsql:equery(Connect, "SELECT teacher FROM classes WHERE class = $1", [Class]),
		        case Data of
				[{Teacher}] ->
		   			Teach = binary:bin_to_list(Teacher),
					{ok, _, [Results]} = epgsql:equery(Connect, "SELECT " ++ DayStart ++ ", " ++ DayEnd ++ " FROM teacher_hours WHERE username = $1", [Teach]),
					epgsql:close(Connect),
					case Results of
						{null, null} -> Reply = <<"No officehours today.">>,
							{reply, Reply, State};
						{<<>>,<<>>} -> Reply = <<"No officehours today.">>,
						       {reply, Reply, State};
						_ ->	{Start, End} = Results,
							Out = "Todays hours begin at " ++ binary:bin_to_list(Start) ++ " and end at " ++ binary:bin_to_list(End) ++ ".",
							Reply = erlang:list_to_binary(Out),
							{reply, Reply, State}
					end;
				_ -> Reply = <<"Not a valid class">>,
				     {reply, Reply, State}
			end;
		_ -> {reply, Hours, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Changes the user password.
%%----------------------------------------------------------------------

handle_call({change_password, Username, CurrentPass, NewPass}, _, State) ->
	Connect = connect_sql(),
	{ok, _, [{KeyBin}]} = epgsql:equery(Connect, "SELECT password FROM users WHERE username = $1", [Username]),
	{ok, Hash} = bcrypt:hashpw(CurrentPass, KeyBin),
	HashBin = erlang:list_to_binary(Hash),
	case HashBin of
		KeyBin ->
			{ok, Salt} = bcrypt:gen_salt(),
			{ok, NewHash} = bcrypt:hashpw(NewPass, Salt),
			Result = epgsql:equery(Connect, "UPDATE users SET password = $1 WHERE username = $2", [NewHash, Username]),
			epgsql:close(Connect),
			case Result of
				{error, _} -> Reply = <<"Unkown error updating password">>,
					      {reply, Reply, State};
				{ok, _} -> Reply = <<"Password Successfully Updated">>,
					   {reply, Reply, State}
			end;
		_ -> 	epgsql:close(Connect),
			Reply = <<"Current Password was incorrect">>,
			{reply, Reply, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Add a new class to the database.
%%----------------------------------------------------------------------

handle_call({add_class, Class, Username}, _, State) ->
	Connect = connect_sql(),
	Result = epgsql:equery(Connect, "INSERT INTO classes(class, teacher) VALUES($1, (SELECT username FROM users WHERE username = $2))", [Class,Username]),
	epgsql:close(Connect),
	case Result of 
		{ok, _} -> Out = "Added " ++ Class ++ " to list of classes",
			   Reply = erlang:list_to_binary(Out),
			   {reply, Reply, State};
		_ -> Reply = <<"Failed to add class">>,
		     {reply, Reply, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Removes a prexisting class from the database.
%%----------------------------------------------------------------------

handle_call({remove_class, Class, Username}, _, State) ->
	Connect = connect_sql(),
	Result = epgsql:equery(Connect, "DELETE FROM classes WHERE class=$1 AND teacher=$2", [Class, Username]),
	epgsql:close(Connect),
	case Result of
		{ok, 1} -> Reply = <<"Successfully Deleted Class">>,
			   {reply, Reply, State};
		_ -> Reply = <<"Failed to delete class">>,
		     {reply, Reply, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Handle a generic call.
%%----------------------------------------------------------------------

handle_call(_, _, State) ->
	{ok, State}.

%%----------------------------------------------------------------------
%% @doc Asynchronous call to the server. Handles logouts, handle ice
%% candidate, and end call.
%%
%% @see gen_server:handle_cast/2
%% @private
%% @end
%%----------------------------------------------------------------------



%%----------------------------------------------------------------------
%% @hidden Logs out the user.
%%----------------------------------------------------------------------

handle_cast({logout, Ip}, State) ->
	Lookup = ets:lookup(usernames, Ip),
	case Lookup of 
		[{_,_}] ->
			[{_,{Username,_}}] = Lookup,
			ets:delete(usernames, Ip),
			ets:delete(logged_on, Username),
			{noreply, State};
		_ -> {noreply, State}
	end;	

%%----------------------------------------------------------------------
%% @hidden Transfer ice candidates from one user to the other.
%%----------------------------------------------------------------------

handle_cast({handle_ice_candidate, Username, Target, Candidate, From}, State) ->
	Lookup = ets:lookup(logged_on, Target),
	case Lookup of 
		[{_,_}] ->
			[{_,{_Ip, PID}}] = Lookup,
			PID ! {new_ice_candidate, Username, Candidate},
			{noreply, State};
		_ ->
			From ! {end_call},
			{noreply, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Informs the other user that the call has ended and needs to 
%% clean up.
%%
%% @doc
%%----------------------------------------------------------------------

handle_cast({end_call, Target}, State) ->
	TargetInfo = ets:lookup(logged_on, Target),
	case TargetInfo of
		[{_,_}] -> 
			[{_,{_,TargetPID}}] = TargetInfo,
			TargetPID ! {end_call},
			{noreply, State};
		_ -> 
			{noreply, State}
	end;

%%----------------------------------------------------------------------
%% @hidden Contact a peer with a server message.
%%----------------------------------------------------------------------

handle_cast({contact_peer, Target, Message}, State) ->
	Lookup = ets:lookup(logged_on, Target),
	case Lookup of 
		[{_,{_,PID}}] ->
			PID ! {server_message, Message};
		_ ->
			ok
	end,
	{noreply, State};

%%----------------------------------------------------------------------
%% @hidden Generic cast call.
%%----------------------------------------------------------------------

handle_cast(_, State) ->
	{noreply, State}.

%%----------------------------------------------------------------------
%% @doc Handles information from the webserver.
%% 
%% @see gen_server:handle_info/2
%% @private
%% @end
%%----------------------------------------------------------------------

handle_info(Info, State) ->
	io:format("Unexpected: ~p~n", [Info]),
	{noreply, State}.

%%----------------------------------------------------------------------
%% @doc Continues the webserver.
%% 
%% @see gen_server:handle_continue/2
%% @private
%% @end
%%----------------------------------------------------------------------

handle_continue(_, State) ->
	{noreply, State}.

%%----------------------------------------------------------------------
%% @doc Terminates the webserver. Deletes the two exisiting tables.
%% 
%% @see gen_server:terminate/2
%% @private
%% @end
%%----------------------------------------------------------------------

terminate(_Reason, _State) ->
	ets:delete(usernames),
	ets:delete(logged_on),
	ok.

%%----------------------------------------------------------------------
%% @doc Details how the code has changed.
%% 
%% @see gen_server:code_change/3
%% @private
%% @end
%%----------------------------------------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.


%%----------------------------------------------------------------------
%% Private Functions
%%----------------------------------------------------------------------


%%----------------------------------------------------------------------
%% @hidden Connects to the sql server.
%%----------------------------------------------------------------------

connect_sql() ->
	{ok, Connect} = epgsql:connect("localhost", "postgres", "postgres", #{
		database=>"officehours", timeout=>4000}),
	Connect.

%%----------------------------------------------------------------------
%% @hidden Handles the comparsion and checking of passwords as well
%% as hashing.
%%
%% @end
%%----------------------------------------------------------------------

compare_login(Password, Keybin, Username, Ip, Type, PID) ->
	{ok, Hash} = bcrypt:hashpw(Password, Keybin),
	HashBin = erlang:list_to_binary(Hash),
	if
		Keybin =:= HashBin ->
			Logged = ets:insert_new(logged_on, {erlang:list_to_binary(Username), {Ip,PID}}),%are you logged on?
			NewIp = ets:insert_new(usernames, {Ip, {erlang:list_to_binary(Username), PID}}),
			Condition = {Logged, NewIp},
			case Condition of 
				{true,true} ->
					Reply = good_login,
					case Type of
						<<"Teacher">> -> 
						 	PID ! {is_teacher};	
						_ -> ok
					end,
					Reply;
				_ -> 
					Reply = bad_login,
					Reply
			end;
		true -> 
			Reply = bad_login,
			Reply
	end.

%%----------------------------------------------------------------------
%% @hidden Insert new classes into the database from a list of classes.
%%----------------------------------------------------------------------

generate_classes(_,[], _) -> ok;

generate_classes(Connect, [Class|Classes], Username) ->
	epgsql:equery(Connect, "insert into classes(class, teacher) values($1, (SELECT username FROM users WHERE username = $2))", [Class,Username]),
	generate_classes(Connect, Classes, Username).


