%%%----------------------------------------------------------------------
%%% @doc day_handler. Handles information related to returning the proper
%%% tuples related to fields in the database corresponding to the correct
%%% day.
%%% 
%%% @author Gabe Kelly
%%% @end
%%%----------------------------------------------------------------------

-module(day_handler).

-export([day_field/1, get_day/1]).


%%----------------------------------------------------------------------
%% @doc Takes a string input and returns a tuple containing the information
%% regarding the corresponding database fields for that day. 
%%
%% @private
%% @end
%%----------------------------------------------------------------------

day_field(Day) ->
	case Day of
		"monday" -> {"monday_start","monday_end"};
		"tuesday" -> {"tuesday_start", "tuesday_end"};
		"wednesday" -> {"wednesday_start", "wednesday_end"};
		"thursday" -> {"thursday_start", "thursday_end"};
		"friday" -> {"friday_start", "friday_end"};
		_ -> throw(bad_day)
	end.

%%----------------------------------------------------------------------
%% @doc Takes an integer input and returns the proper database fields
%% to use for the day. If the integer is less than 1 or greater than
%% 5 it returns a binary statement.
%%
%% @private
%% @end
%%----------------------------------------------------------------------

get_day(DayNum) ->
	case DayNum of
		1 -> {"monday_start","monday_end"};
		2 -> {"tuesday_start", "tuesday_end"};
		3 -> {"wednesday_start", "wednesday_end"};
		4 -> {"thursday_start", "thursday_end"};
		5 -> {"friday_start", "friday_end"};
		_ -> <<"No hours today">>
	end.
