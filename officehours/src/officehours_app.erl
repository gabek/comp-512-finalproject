%%%----------------------------------------------------------------------
%%% @doc The application module of Officehours. Responsible for starting
%%% the cowboy server and ensuring that dependencies start.
%%%
%%% @reference Partially modeled after erlbus chat room example at:
%%% https://github.com/cabol/erlbus/tree/master/examples/chat
%%%
%%% @author Gabe Kelly
%%% @end
%%%----------------------------------------------------------------------

-module(officehours_app).
-behaviour(application).

-export([start/0,start/2]).
-export([stop/1]).

%%----------------------------------------------------------------------
%% @doc Starts all of the dependencies that officehours depends upon.
%%
%% @end
%%----------------------------------------------------------------------
-spec start() -> {ok, _} | {error, term()}.
start() ->
	application:ensure_all_started(officehours).

%%----------------------------------------------------------------------
%% @doc Starts the cowboy HTTPS server. Also establishes link to the supervisor.
%% @private
%% @see application:start/2.
%% @end
%%----------------------------------------------------------------------
start(_, _) ->
	Dispatch = cowboy_router:compile([{'_', [
			{"/", cowboy_static, {priv_file, officehours, "index.html"}},
			{"/websocket", websocket_handler, []},
			{"/static/[...]", cowboy_static, {priv_dir, officehours, "static"}}
		]}
	]),
	PrivDir = code:priv_dir(officehours),
	{ok, _} = cowboy:start_tls(https, [
			{port, 8443},
			{cacertfile, PrivDir ++ "/ssl/ca-officehours.crt"},
			{certfile, PrivDir ++ "/ssl/officehours.crt"},
			{keyfile, PrivDir ++ "/ssl/officehours.key"}
		], #{env => #{dispatch => Dispatch}}),
	officehours_sup:start_link().


%%----------------------------------------------------------------------
%% @doc Halts the application.
%% @private
%% @see application:stop/1
%% @end
%%----------------------------------------------------------------------
stop(_) ->
	ok.
