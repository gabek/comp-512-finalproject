%%%----------------------------------------------------------------------
%%% @doc parser_backend. Handles all expected cases of json coming in from
%%% the client.
%%% @reference Use of proplists was not my own idea. https://stackoverflow.com/questions/19109355/erlang-convert-complex-binary-structure-to-json.
%%% 
%%% @author Gabe Kelly
%%% @end
%%%----------------------------------------------------------------------


%%----------------------------------------------------------------------
%% @todo Roll all non json parses into their own proplist decoder.
%%----------------------------------------------------------------------

-module(parser_backend).

-export([parse_json/1, parse_create/1, parse_login/1, parse_candidate/1, parse_connection/1,
parse_hours/1, parse_changes/1, parse_class/1, parse_message/1]).


%%----------------------------------------------------------------------
%% @doc Uses mochijson2 to decode a incoming json message. Expects the format
%% {type:"type",value:"value"}.
%% @see mochijson2:decode/1
%% @end
%%----------------------------------------------------------------------

parse_json(Msg) ->
	Struct = mochijson2:decode(Msg),
	{struct, Data} = Struct,
	Type = proplists:get_value(<<"type">>, Data),
	Value = proplists:get_value(<<"value">>, Data),
	{Type, Value}.

%%----------------------------------------------------------------------
%% @hidden Parses information related to a chatroom message.
%%----------------------------------------------------------------------

parse_message(Struct) ->
	{struct, Data} = Struct,
	Username = proplists:get_value(<<"user">>, Data),
	Text = proplists:get_value(<<"text">>, Data),
	{Text, Username}.

%%----------------------------------------------------------------------
%% @hidden Parses information related to creating a user.
%%----------------------------------------------------------------------

parse_create(Struct) ->
	{struct, Data} = Struct,
	FirstName = proplists:get_value(<<"first">>, Data),
	LastName = proplists:get_value(<<"last">>, Data),
	Password = proplists:get_value(<<"pass">>, Data),
	IdNum = proplists:get_value(<<"id">>, Data),
	{FirstName, LastName, Password, IdNum}.

%%----------------------------------------------------------------------
%% @hidden Parses information related to logging in.
%%----------------------------------------------------------------------

parse_login(Struct) ->
	{struct, Data} = Struct,
	Username = proplists:get_value(<<"username">>, Data),
	Password = proplists:get_value(<<"pass">>, Data),
	Class = proplists:get_value(<<"classroom">>, Data),
	{Username, Password, Class}.

%%----------------------------------------------------------------------
%% @hidden Parses information related to ice candidates.
%%----------------------------------------------------------------------

parse_candidate(Struct) ->
	{struct, Data} = Struct,
	Username = proplists:get_value(<<"name">>, Data),
	Target = proplists:get_value(<<"target">>, Data),
	Candidate = proplists:get_value(<<"candidate">>, Data),
	{Username,Target, Candidate}.

%%----------------------------------------------------------------------
%% @hidden Parses information related to sdp connections.
%%----------------------------------------------------------------------

parse_connection(Struct) ->
	{struct, Data} = Struct,
	Username = proplists:get_value(<<"name">>, Data),
	Target = proplists:get_value(<<"target">>, Data),
	SDP = proplists:get_value(<<"sdp">>, Data),
	{Username, Target, SDP}.

%%----------------------------------------------------------------------
%% @hidden Parses proplists related to submission of teacher times.
%%----------------------------------------------------------------------

parse_hours(Struct) ->
	{struct, Data} = Struct,
	Day = proplists:get_value(<<"day">>, Data),
	Start = proplists:get_value(<<"starttime">>, Data),
	End = proplists:get_value(<<"endtime">>, Data),
	Username = proplists:get_value(<<"username">>, Data),
	{Day, Start, End, Username}.

%%----------------------------------------------------------------------
%% @hidden Parses proplists related to changing a password.
%%----------------------------------------------------------------------

parse_changes(Struct) ->
	{struct, Data} = Struct,
	Username = proplists:get_value(<<"username">>, Data),
	CurrentPass = proplists:get_value(<<"current">>, Data),
	NewPass = proplists:get_value(<<"new_password">>, Data),
	{binary:bin_to_list(Username), binary:bin_to_list(CurrentPass), binary:bin_to_list(NewPass)}.

%%----------------------------------------------------------------------
%% @hidden Parses proplists related to adding a new class.
%%----------------------------------------------------------------------

parse_class(Struct) ->
	{struct, Data} = Struct,
	Username = proplists:get_value(<<"username">>, Data),
	Class = proplists:get_value(<<"classroom">>, Data),
	{binary:bin_to_list(Class), binary:bin_to_list(Username)}.
