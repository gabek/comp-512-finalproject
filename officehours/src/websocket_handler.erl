%%%----------------------------------------------------------------------
%%% @doc websocket_handler. The cowboy handler for the websocket connection.
%%% This module will take json data coming from the client and process the data.
%%% This module will also call the webserver to manipulate certain data.
%%%
%%% @reference Use erlbus chat room example as a basis for this code. Can be found at:
%%% https://github.com/cabol/erlbus/tree/master/examples/chat
%%% 
%%% @author Gabe Kelly
%%% @end
%%%----------------------------------------------------------------------

-module(websocket_handler).

-export([init/2]).

%%----------------------------------------------------------------------
%% Call backs for cowboy_websocket.
%%----------------------------------------------------------------------

-export([websocket_init/1, websocket_handle/2, websocket_info/2, terminate/3]). 

%%----------------------------------------------------------------------
%% A State record. Used to store information about our client.
%% Information includes the current class, the Erlbus handler, the ip of the client,
%% and who they are calling (if anyone).
%%----------------------------------------------------------------------

-record(state, {class, handler, ip, target}).

%%----------------------------------------------------------------------
%% @doc Called automatically when a client connects to the server.
%% Specifies this module as a cowboy_websocket to start.
%% Takes in A Request as Req. Which is information about a client.
%%
%% @private
%% @see cowboy_websocket:init/2
%% @end
%%----------------------------------------------------------------------
init(Req, _) ->
	Opts = #{idle_timeout => 3600000},
	{cowboy_websocket, Req, #state{ip=get_ip(Req)}, Opts}.

%%----------------------------------------------------------------------
%% @doc Initiates the ebus handler, and passes it to the state.
%%
%% @private
%% @see cowboy_websocket:websocket_init/1
%% @end
%%----------------------------------------------------------------------

websocket_init (State) ->
	Handler = ebus_proc:spawn_handler(fun handle_msg/2, [self()]),
	{ok, #state{handler = Handler, ip = State#state.ip}}.

%%----------------------------------------------------------------------
%% @doc Handles incoming text (in the form of json) from the client.
%% The json will be parsed and from it a webserver function will be called 
%% based on the type value of the json.
%%
%% @private
%% @see cowboy_websocket:websocket_handle/2
%% @end
%%----------------------------------------------------------------------

websocket_handle({text, Msg}, State) ->
	{Type, Value} = parser_backend:parse_json(Msg),
	case Type of
		<<"message">> ->
			{Text, Username} = parser_backend:parse_message(Value),
			case Username of
				[] -> ok;
				null -> ok;
				_ -> ebus:pub(State#state.class, {Username, Text})
			end,
			{ok, State};
		<<"initiate">> ->
			self() ! {initiate_call},
			{ok, State};
		<<"create">> ->
			{Firstname, Lastname, Password, IdNum} = parser_backend:parse_create(Value),
			Userinfo = webserver:create_student(binary:bin_to_list(Firstname), binary:bin_to_list(Lastname), binary:bin_to_list(Password), binary:bin_to_list(IdNum)),
			self() ! {server_message, erlang:list_to_binary(Userinfo)},
			{ok, State};
		<<"submit_hours">> ->
			{Day, Start, End, Username} = parser_backend:parse_hours(Value),
			Reply = webserver:submit_hours(binary:bin_to_list(Day), binary:bin_to_list(Start), binary:bin_to_list(End), binary:bin_to_list(Username)),
			self() ! {hours_submit, Reply},
			{ok, State};
		<<"check_hours">> ->
			Reply = webserver:check_hours(Value, binary:bin_to_list(State#state.class)),
			self() ! {server_message, Reply},
			{ok, State};
		<<"add-class">> ->
			{Class, Username} = parser_backend:parse_class(Value),
			Reply = webserver:add_class(Class, Username),
			self() ! {server_message, Reply},
			{ok, State};
		<<"remove-class">> ->
			{Class, Username} = parser_backend:parse_class(Value),
			Reply = webserver:remove_class(Class, Username),
			case Reply of 
				<<"Successfully Deleted Class">> -> ebus:pub(erlang:list_to_binary(Class), {server, <<"The class you are currently in has been deleted">>}),
								    ebus:pub(erlang:list_to_binary(Class), {server, <<"If you leave, you won't be able to join again">>});
				_ -> ok
			end,
			self() ! {server_message, Reply},
			{ok, State};
		<<"login">> ->
			{Username, Password, Class} = parser_backend:parse_login(Value),
			Login = webserver:login(State#state.ip, binary:bin_to_list(Username), binary:bin_to_list(Password), binary:bin_to_list(Class)),
			self() ! {login, Login, Class},
			case Login of
				good_login ->	ebus:sub(State#state.handler, Class),
						{ok, #state{class=Class, handler = State#state.handler, ip = State#state.ip, target=null}};
				_ -> {ok,State}
			end;
		<<"logout">> ->
			webserver:logout(State#state.ip),
			ebus:unsub(State#state.handler, State#state.class),
			case State#state.target of
				null -> ok;
				_ -> webserver:end_call(State#state.target)
			end,
			{ok, #state{class=null, handler = State#state.handler, ip = State#state.ip, target=null}};
		<<"switch-class">> ->
			ClassId = binary:bin_to_list(Value),
			Reply = webserver:switch_class(ClassId),
			case Reply of
				success ->
					erlang:display("success"),
					ebus:unsub(State#state.handler, State#state.class),
					ebus:sub(State#state.handler, ClassId),
					erlang:display("Classroom changed"),
					self() ! {switch_class, Reply, ClassId},
					erlang:display("Message sent"),
					{ok, #state{class=ClassId, handler = State#state.handler, ip=State#state.ip, target=State#state.target}};
				_ ->
					erlang:display("Failure :("),
					self() ! {switch_class, Reply, <<"">>},
					erlang:display("Message sent"),
					{ok, State}
			end;
		<<"initiate_call">> ->
			Reply = webserver:contact_teacher(Value, State#state.class),
			erlang:display(Reply),
			self() ! {server_message, Reply},
			{ok, State};
		<<"busy">> ->
			webserver:contact_peer(Value, <<"The teacher is currently in a Call.">>),
			{ok, State};
		<<"candidate">> ->
			{Username, Target,Candidate} = parser_backend:parse_candidate(Value),
			webserver:handle_ice_candidate(Username, Target, Candidate, self()),
			{ok, State};
		<<"call-offer">> ->
			erlang:display("A negotiation has been received"),
			{Username, Target, SDP} = parser_backend:parse_connection(Value),
			webserver:call_offer(Username, Target, SDP),
			{ok, #state{class=State#state.class, handler = State#state.handler, ip = State#state.ip, target=Target}};
		<<"call-answer">> ->
			{_, Target, SDP} = parser_backend:parse_connection(Value),
			webserver:call_answer(Target, SDP),
			{ok, #state{class=State#state.class, handler = State#state.handler, ip = State#state.ip, target=Target}};
		<<"end-call">> ->
			webserver:end_call(Value),
			{ok, #state{class=State#state.class, handler = State#state.handler, ip = State#state.ip, target=null}};
		<<"partial-failure">> ->
			webserver:contact_peer(Value, <<"The peer you were communicating with failed a step.">>),
			webserver:end_call(Value),
			{ok, State};
		<<"change">> ->
			{Username, CurrentPass, NewPass} = parser_backend:parse_changes(Value),
			Reply = webserver:change_password(Username, CurrentPass, NewPass),
			self() ! {server_message, Reply},
			{ok, State};
		_ -> erlang:display("Unknown command"),
			erlang:display(Type),
			{ok, State}
	end;
%%----------------------------------------------------------------------
%% A generic call to websocket_handle. Handles generic cases and displays
%% the information for debugging purposes.
%%----------------------------------------------------------------------

websocket_handle(Debug, State) ->
	erlang:display(Debug),
	{ok, State}.

%%----------------------------------------------------------------------
%% @doc Receives info that is sent by another process. The primary use is
%% to dispatch messages across the websocket to the associated client. An atom
%% is used to differentiate the type of messages. 
%%
%% @private
%% @see cowboy_websocket:websocket_info/2
%% @end
%%----------------------------------------------------------------------


%----------------------------------------------------------------------
% A message has been published to the chatroom.
%----------------------------------------------------------------------

websocket_info({message_published, {Sender, Msg}}, State) ->
	{reply, {text, mochijson2:encode({[{type, message}, {sender, Sender}, {msg, Msg}]})}, State};

%----------------------------------------------------------------------
% A teacher has accepted a call and it is starting.
%----------------------------------------------------------------------

websocket_info({initiate_call}, State) ->
	{reply, {text, mochijson2:encode({[{type, callstart}]})}, State};

%----------------------------------------------------------------------
% Results from a login attempt.
%----------------------------------------------------------------------

websocket_info({login, Message, Class}, State) ->
	{reply, {text, mochijson2:encode({[{type, login_response}, {message, Message}, {classroom, Class}]})}, State};

%----------------------------------------------------------------------
% Results from trying to change the class
%----------------------------------------------------------------------

websocket_info({switch_class, Result, ClassId}, State) ->
	erlang:display("Message sent"),
	{reply, {text, mochijson2:encode({[{type, switch_class}, {result, Result}, {classroom, ClassId}]})}, State};



%----------------------------------------------------------------------
% Information to contact a teacher about who is calling.
%----------------------------------------------------------------------

websocket_info({contact_teacher, StudentName}, State) ->
	{reply, {text, mochijson2:encode({[{type, request_call},{student_name, StudentName}]})}, State};

%----------------------------------------------------------------------
% Information about a new Ice Candidate to the peer.
%----------------------------------------------------------------------

websocket_info({new_ice_candidate, Username, Candidate}, State) ->
	{reply, {text, mochijson2:encode({[{type,new_ice_candidate}, {from, Username}, {candidate, Candidate}]})}, State};

%----------------------------------------------------------------------
% Contains a call offer. Sent from teacher to student.
%----------------------------------------------------------------------

websocket_info({call_offer, Username, SDP}, State) ->
	{reply, {text, mochijson2:encode({[{type,call_offer}, {from, Username}, {sdp, SDP}]})}, State};

%----------------------------------------------------------------------
% Returns an answer to a call offer. Sent from student to teacher.
%----------------------------------------------------------------------

websocket_info({call_answer, SDP}, State) ->
	{reply, {text, mochijson2:encode({[{type,call_answer}, {sdp, SDP}]})}, State};

%----------------------------------------------------------------------
% Informs a peer that the call has ended.
%----------------------------------------------------------------------

websocket_info({end_call}, State) ->
	{reply, {text, mochijson2:encode({[{type,end_call}]})}, #state{class=State#state.class, handler = State#state.handler, ip = State#state.ip, target=null}};

%----------------------------------------------------------------------
% Information from the server confirming that they have the teacher role.
%----------------------------------------------------------------------

websocket_info({is_teacher}, State) ->
	{reply, {text, mochijson2:encode({[{type,is_teacher}]})}, State};

%----------------------------------------------------------------------
% Returns information to the teacher about hours submitted.
%----------------------------------------------------------------------
websocket_info({hours_submit, Status}, State) ->
	{reply, {text, mochijson2:encode({[{type,hours_submit}, {status, Status}]})}, State};

%----------------------------------------------------------------------
% A handler for a generic server message. 
%----------------------------------------------------------------------

websocket_info({server_message, Text}, State) ->
	{reply, {text, mochijson2:encode({[{type, server_message}, {text, Text}]})}, State};

%----------------------------------------------------------------------
% A handler for an unexpected info to be passed. Displays it for the server to see.
%----------------------------------------------------------------------

websocket_info(Info, State) ->
	erlang:display("Unknown call received"),
	erlang:display(Info),
 	{ok, State}.

%%----------------------------------------------------------------------
%% @doc Called upon termination of a websocket. Automatically calls logout on the
%% ip to ensure at least once property for removing the user. Will also display
%% reasons for termination.
%%
%% @private
%% @see cowboy_websocket:terminate/3
%% @end
%%----------------------------------------------------------------------
terminate(Reason, Req, State) ->
	erlang:display(Reason),
	erlang:display(Req),
	webserver:logout(State#state.ip),
	case State#state.target of
		null -> ok;
		_ -> webserver:end_call(State#state.target)
	end,
	ebus:unsub(State#state.handler, State#state.class),
	ok.

%%----------------------------------------------------------------------
%% @hidden Automatically called when a new message is published to the chat.
%% Each subscriber to a class room receives a message.
%% @end
%%----------------------------------------------------------------------

handle_msg(Msg, Context) ->
	Context ! {message_published, Msg}.

%%----------------------------------------------------------------------
%% @hidden Retrieves the users ip host from the request.
%%----------------------------------------------------------------------

get_ip(Req) ->
	{Host, _} = cowboy_req:peer(Req),
	Name = list_to_binary([inet_parse:ntoa(Host)]),
	Name.
