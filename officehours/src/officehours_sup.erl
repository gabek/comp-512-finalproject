-module(officehours_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
	{ok, {{one_for_one, 5, 60}, [{webserver, {webserver, start_link, []},
					transient, 60000, worker, [webserver]}]}}.
